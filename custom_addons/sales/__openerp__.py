# -*- coding: utf-8 -*-
{
    'name': "Sales Customization",

    'summary': """
        Customized Sales.""",

    'description': """
        To have customized Sales that suits the custom business.
    """,

    'author': "DRC Systems",
    'website': "http://www.drcsystems.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale', 'account'],

    # always loaded
    'data': [
        'views/quotation_view_report.xml',
        'views/invoice_view_report.xml',
        'views/sale_order_custom_form.xml',
        'views/invoice_custom_form.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
