# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import UserError
from openerp.tools.amount_to_text_en import amount_to_text


class Order(models.Model):
    _inherit = 'sale.order'

    pre_carriage_by = fields.Char()
    place_of_receipt_by_pre_carrier = fields.Char()
    country_of_origin = fields.Many2one('res.country', 'Country Origin')
    country_of_destination = fields.Many2one('res.country')
    terms_of_delivery_and_payment = fields.Text(string='Terms of delivery & payment')
    flight_no = fields.Char('Vessel/Flight No')
    port_loading = fields.Char('Port of Loading')
    port_discharge = fields.Char('Port of Discharge')
    final_destination = fields.Char('Final Destination')

    net_wt = fields.Char()
    gross_wt = fields.Char()
    quantity = fields.Char()
    batch_no = fields.Char()
    mfg_dt = fields.Datetime()
    exp_dt = fields.Datetime()

    incoterm = fields.Char()
    packing = fields.Char()
    lead_time = fields.Char()
    quot_country_of_origin = fields.Many2one('res.country', 'Country Origin')
    delivery = fields.Char()
    transit_time = fields.Datetime()

    @api.multi
    def _prepare_invoice(self):
        """
        overridden to have extra value while creating invoice from sale order.
        """
        # Call super to have existing values
        invoice_values = super(Order, self)._prepare_invoice()

        # Update the dictionary to have extra values
        invoice_values.update({
            'pre_carriage_by': self.pre_carriage_by,
            'place_of_receipt_by_pre_carrier': self.place_of_receipt_by_pre_carrier,
            'country_of_origin': self.country_of_origin.id,
            'country_of_destination': self.country_of_destination.id,
            'terms_of_delivery_and_payment': self.terms_of_delivery_and_payment,
            'date_order': self.date_order,
            'partner_shipping_id': self.partner_shipping_id.id,
            'flight_no': self.flight_no,
            'port_loading': self.port_loading,
            'port_discharge': self.port_discharge,
            'final_destination': self.final_destination,
            'net_wt': self.net_wt,
            'gross_wt': self.gross_wt,
            'quantity': self.quantity,
            'batch_no': self.batch_no,
            'mfg_dt': self.mfg_dt,
            'exp_dt': self.exp_dt,
        })

        return invoice_values


class Invoice(models.Model):
    _inherit = 'account.invoice'

    pre_carriage_by = fields.Char()
    place_of_receipt_by_pre_carrier = fields.Char()
    country_of_origin = fields.Many2one('res.country')
    country_of_destination = fields.Many2one('res.country')
    terms_of_delivery_and_payment = fields.Text(string='Terms of delivery & payment')
    date_order = fields.Date()
    partner_shipping_id = fields.Many2one('res.partner')
    amount_in_words = fields.Char(compute='_get_amount_in_words')
    flight_no = fields.Char('Vessel/Flight No')
    port_loading = fields.Char('Port of Loading')
    port_discharge = fields.Char('Port of Discharge')
    final_destination = fields.Char('Final Destination')
    net_wt = fields.Char('NET WT')
    gross_wt = fields.Char('GROSS WT')
    quantity = fields.Char('QUANTITY')
    batch_no = fields.Char('BATCH NO')
    mfg_dt = fields.Datetime('MFG DT')
    exp_dt = fields.Datetime('EXP DT')

    @api.depends('amount_total')
    def _get_amount_in_words(self):
        for invoice in self:
            invoice.amount_in_words = amount_to_text(invoice.amount_total, 'en', 'US DOLLARS')

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    name = fields.Html(string='Description', required=True)

    no_kind_of_pkgs = fields.Text(string='No. & Kind of Pkgs', required=True)
    tare_weight = fields.Char(string='TARE Weight(IN KGS.)', required=True, default=0)
    gross_weight = fields.Char(string='GROSS Weight(IN KGS.)', required=True, default=0)
