/**
 * PDF page settings.
 * Must have the correct values for the script to work.
 * All numbers must be in inches (as floats)!
 * Use google to convert margins from mm to in ;) 
 * 
 * @type {Object}
 */
var pdfPage = {
    width: 8.26771654, // inches, 210mm
    height: 11.6929134, // inches, 296mm
    margins: {
        top: 0.19685, left: 0.275591, 
        right: 0.275591, bottom: 0 
    }
};

/**
 * The distance to bottom of which if the element is closer, it should moved on 
 * the next page. Should be at least the element (TR)'s height.
 * 
 * @deprecated Now it is automatically detected from the TR's height, no longer needed.
 * @type {Number}
 */
var splitThreshold = 1;

/**
 * Class name of the tables to automatically split.
 * Should not contain any CSS definitions because it is automatically removed 
 * after the split.
 * 
 * @type {String}
 */
var splitClassName = 'inner_page';

/**
 * Set to true to enable visual debugging of the page dimensions via HTML elements / text.
 */
var visualDebug = false;


/**
 * Window load event handler.
 * We use this instead of DOM ready because webkit doesn't load the images yet.
 */
$(window).load(function () {
    // get document resolution
    var dpi = $('<div id="dpi"></div>')
        .css({
            height: '1in', width: '1in',
            top: '-100%', left: '-100%',
            position: 'absolute'
        })
        .appendTo('body')
        .height();
    
    // page height in pixels
    var pageHeight = Math.floor(
        (pdfPage.height - pdfPage.margins.top - pdfPage.margins.bottom) * dpi);
    
    // temporary set body's width and padding to match pdf's size
    var $body = $('body');
    $body.css('width', Math.floor((pdfPage.width - pdfPage.margins.left - pdfPage.margins.right)*dpi)+'px');
    $body.css('padding-left', Math.floor(pdfPage.margins.left*dpi)+'px');
    $body.css('padding-right', Math.floor(pdfPage.margins.right*dpi)+'px');
    //$body.css('padding-top', Math.floor(pdfPage.margins.top*dpi)+'px');
    $body.css('padding-top', 0);
    
    // DEBUG: show the page height (must be an exact fit to the page's content area in order for the script to work)
    if (visualDebug) {
        $body.append('<div id="debug_div" style="position: absolute; top: 0; height:' + (pageHeight - 2) + 'px; ' + 
                'right: 0; border: 1px solid #FF0000; background: blue; color: white;">Test<br />' + pageHeight + '<br /></div>');
        $('#debug_div').append( $('#debug_div').offset().top + '');
    }
    
    /* 
     * Cycle through all tables and split them in two if necessary.
     * We need this in a loop for it to work for tables spanning multiple pages:
     * first, the table is split in two; then, if the second table also spans multiple 
     * pages, it is also split and so on until there are no more.
     * Because when modifying the upper tables, the elements' positions will change, 
     * we need to maintain an offset correction value.
     * 
     * This method can be used for all document's elements (not just tables), but the 
     * overhead would be too big. Use CSS's `page-break-inside: avoid` which works for
     * divs and many other block elements.
     */
    var tablesModified = true;
    var offsetCorrection = 0;
    var headerHeight = $('div.page > div.header').height();
    var footerHeight = $('div.page > div.footer').height();

    while (tablesModified) {
        tablesModified = false;
        
        $('.'+splitClassName).each(function(){
            var $t = $(this);
            
            // clone the original table
            var copy = $t.clone();
            copy.find('div.product > div.row').remove();
            var $cbody = copy.find('div.product');
            var found = false;
            $t.removeClass(splitClassName); // for optimisation
            
            var newOffsetCorrection = offsetCorrection;
            var CF_total = CF_quantity = 0;
            var CF_found = 0;
            $('div.product > div.row', $t).each(function(){
                var $tr = $(this);
                
                // compute element's top position and page's end
                var top = $tr.offset().top;
                var ctop = offsetCorrection + top;
                var pageEnd = (Math.floor(ctop/pageHeight)+1)*pageHeight;
                pageEnd = pageEnd - footerHeight;
                // DEBUG: prints TR's top and the current page end inside its first column
                if (visualDebug) {
                    //if (Math.random() > 0.7)
                    //  $tr.find('td:first').append('<br /> MULTI!');
                    $tr.find('div:first').prepend('<div style="position: absolute;  z-index:2; background: #EEE; padding: 2px;" class="debug">' +
                        ctop + ' / ' + pageEnd + '/ off=' + offsetCorrection + ' / h=<span class="tr-height">-</span>px' + '</div>' );
                }
                
                // check whether the current element is close to the page's end. 
                // dynamic threshold
                var threshold = splitThreshold;
                if ($tr.height() > threshold)
                    threshold = $tr.height() + 10;
                if (visualDebug) {
                    $tr.find('.tr-height').text($tr.height());
                    $tr.find('.tr-height').parent().append('<br/><span>f='+found+'/ctop='+ctop+'/pageend='+pageEnd+'/threshold'+threshold+'</span>');
                }
                if (found || (ctop >= (pageEnd - threshold))) {
                    // move the element to the cloned table
                    $tr.detach().appendTo($cbody);
                    if (visualDebug) $tr.find('span.debug').append(' D!');
                    
                    if (!found) {
                        // compute the new offset correction
                        newOffsetCorrection += (pageEnd - ctop);
                    }
                    found = true;
                    CF_found += 1;
                } else {
                    CF_total += parseFloat($tr.find('div.price').html());
                    CF_quantity += parseFloat($tr.find('div.quantity').html());

                }
                if (CF_found == 1) {
                    var $clone_tr = $tr.clone();
                    $clone_tr.find('.pkgs').empty();
                    $clone_tr.find('.name').html('<strong>C/F</strong>');
                    $clone_tr.find('.quantity').text(CF_quantity || '');
                    $clone_tr.find('.unit').html('&nbsp;');
                    $clone_tr.find('.price').text(CF_total || '');
                    $clone_tr.prependTo($cbody);
                }
            });
            
            // if the cloned table has no contents...
            if (!found) 
                return;
            // add footer at last of page
            var $footer = $('div.page > div.footer').clone()
            if (found) {
                $footer.find('.amount_in_words').html('<b>CARRY FORWARD</b>');
                $footer.find('.footer_total strong').text(CF_total || '');
                $t.append($footer);
            }
            // else {
            //     $t.append($footer);
            // }
                // $t.find('div.product > div.row:last').parent().parent().css('min-height', Math.floor(pageEnd - headerHeight - footerHeight)+'px');
                // $footer.insertAfter($t)
            
            offsetCorrection = newOffsetCorrection;
            tablesModified = true;
            // $t.css('height', Math.floor(pageHeight - headerHeight - footerHeight)+'px');
            // $t.css('min-height', Math.floor(pageHeight - headerHeight - footerHeight)+'px');
            // $t.css('max-height', Math.floor(pageHeight - headerHeight - footerHeight)+'px');
            // $t.append($('div.page > div.footer').clone());

            // add a page-breaking div 
            // (with some whitespace to correctly show table top border)
            var $br = $('<div style="height: '+headerHeight+'px;"></div>')
                .css('page-break-before', 'always');
            $br.insertAfter($t);
            copy.css('height', Math.floor(pageHeight - headerHeight - footerHeight -20)+'px');
            copy.css('min-height', Math.floor(pageHeight - headerHeight - footerHeight -20)+'px');
            copy.css('max-height', Math.floor(pageHeight - headerHeight - footerHeight -20)+'px');
            copy.insertAfter($br);

            if (visualDebug) {
                $t.css('background-color', 'green');
                $br.css('background-color', 'red');
            }
            // $('.'+splitClassName).css('max-height', Math.floor(pageHeight - headerHeight - footerHeight)+'px');
        });
    }
    
    // restore body's padding
    // $body.css('padding-left', 0);
    // $body.css('padding-right', 0);
    // $body.css('padding-top', 0);
});